

const add = {};


add.getModels = function () {
    var element = document.getElementById("brand");
    var id = element.options[element.selectedIndex].value;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "getModels");
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            document.getElementById("model_div").innerHTML = "<select name='model'>" + xmlhttp.responseText + "</select>";
        }
    };
    xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send("id=" + id);

};

add.submitForm = function () {
    let brand = document.getElementById("brand");
    if (brand.options[brand.selectedIndex].value === "") {
        document.getElementById("brandError").classList.remove("hidden");
        return false;
    }

    let cm3 = document.getElementById("cm3");
    if (cm3.options[cm3.selectedIndex].value === "") {
        document.getElementById("cm3Error").classList.remove("hidden");
        return false;
    }

    let mileage = document.getElementById("mileage").value;
    if (mileage === "") {
        document.getElementById("mileageError").classList.remove("hidden");
        return false;
    }
    if(isNaN(mileage)){
        alert("for mileage you must enter a number");
        return false;
    }
    
    let price = document.getElementById("price").value;
    if(price === ""){
        document.getElementById("priceError").classList.remove("hidden");
        return false;
    }
    if(isNaN(price)){
        alert("for price you must enter a number");
        return false;
    }
    let phone = document.getElementById("phone").value;
    if(phone === ""){
        document.getElementById("phoneError").classList.remove("hidden");
        return false;
    }
    if(isNaN(phone)){
        alert("for phone you must eneter a number");
        return false;
    }
    document.getElementById("formAddNewCar").submit();
};


