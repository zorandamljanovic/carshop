
/* id */

const signup = {};

signup.checkEmail = function () {
    let email = document.getElementById("femail").value;
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "checkData");
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            if (xmlhttp.responseText === "Existing") {
                document.getElementById("emailMessage").style.display = "block";
                document.getElementById("emailMessage").innerHTML = "Element allready existing in database";
            } else {
                document.getElementById("emailMessage").style.display = "none";
            }
        }
    };
    xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send("email=" + email);

};

signup.checkUsername = function () {
    let username = document.getElementById("fusername").value;
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "checkData");
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            if (xmlhttp.responseText === "Existing") {
                document.getElementById("usernameMessage").style.display = "block";
                document.getElementById("usernameMessage").innerHTML = "Username allready existing in database";
            } else {
                document.getElementById("usernameMessage").style.display = "none";
            }
        }
    };
    xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send("username=" + username);
};