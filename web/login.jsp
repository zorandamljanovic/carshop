<%-- 
    Document   : login
    Created on : Dec 10, 2019, 10:39:33 AM
    Author     : zdy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://kit.fontawesome.com/f922825c19.js"></script>
        <link rel = "shortcut icon" href = "logo.png" type = "image/x-icon"> 
        <title>Zoran used cars</title>
    </head>
    <body>

        <%

            if (request.getSession().getAttribute("name") != null) {

        %>
        <nav class="nav-bar">
            <div class="homePage">
                <a href="indexData" title="Home"><i class="fas fa-home"></i></a>
            </div>
            <div class="login-signup">
                <a href="signup.jsp">Signup</a>
                <a href="logout">Logout</a>
            </div>
        </nav>

        <%            out.print("You are already logged");
        } else {
        %>
        <nav class="nav-bar">
            <div class="homePage">
                <a href="indexData" title="Home"><i class="fas fa-home"></i></a>
            </div>
            <div class="login-signup">
                <a href="signup.jsp">Signup</a>
            </div>
        </nav>
        <div class="login-box"><h1>Login</h1>
            <form method="post" action="login">
                <div class="username-div">
                    <i class="fas fa-user"></i>
                    <input type="text" name="username" autocomplete="off" placeholder="Username"/><br/>
                </div>
                <div class="password-div">
                    <i class="fas fa-lock"></i>
                    <input type="password" name="password" placeholder="Password"/><br/>
                </div>
                <c:choose>
                    <c:when test="${errMessage == 'notExisting'}">
                        <div id="userNotExists" style="color: red">Something wrong with username or password</div>
                    </c:when>
                </c:choose>
                <input class="button" type="submit" value="Login" name="loginButton">
            </form>
        </div>
        <%
            }
        %>
    </body>
</html>
