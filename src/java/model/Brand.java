package model;

import servlets.HibernateUtil;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.Query;
import org.hibernate.Session;

@Entity
@Table(name = "brands")
public class Brand {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "name")
    private String name;
    @OneToMany(targetEntity = Model.class)
    @JoinColumn(name = "brand_id")
    private List<Model> model;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the model
     */
    public List<Model> getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(List<Model> model) {
        this.model = model;
    }

    public static List getAllBrands() {
        List<Brand> brands;
        Session session = HibernateUtil.createSessionFactory().openSession();
        Query query = session.createQuery("from Brand");
        brands = query.list();
        return brands;
    }

    public static List getBrandName(int id) {
        List<Brand> brands;
        Session session = HibernateUtil.createSessionFactory().openSession();
        Query query = session.createQuery("from Brand where id = " + id);
        brands = query.list();
        return brands;
    }

    public static String getBrandName(String brandParam) {
        String pathFolder = "";
        int id = Integer.parseInt(brandParam);
        List<Brand> brands = Brand.getBrandName(id);
        for (Brand brand : brands) {
            pathFolder = brand.getName();
        }
        return pathFolder;
    }

    public static Brand getBrandById(int id) {
        Brand brand = new Brand();
        Session session = HibernateUtil.createSessionFactory().openSession();
        Query query = session.createQuery("from Brand brand where brand.id = :id");
        query.setParameter("id", id);
        try{
            brand = (Brand) query.list().get(0);
        }catch(Exception e){
            return null;
        }
        session.close();
        return brand;
    }
}
