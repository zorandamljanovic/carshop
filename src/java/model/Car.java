package model;

import java.util.List;
import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.annotations.Cascade;
import servlets.HibernateUtil;

@Entity
@Table(name = "cars")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "model_id", referencedColumnName = "id")
    private Model model;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "brand_id", referencedColumnName = "id")
    private Brand brand;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "detail_id", referencedColumnName = "id")
    private Detail detail;

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the model
     */
    public Model getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(Model model) {
        this.model = model;
    }

    /**
     * @return the brand
     */
    public Brand getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    /**
     * @return the detail
     */
    public Detail getDetail() {
        return detail;
    }

    /**
     * @param detail the detail to set
     */
    public void setDetail(Detail detail) {
        this.detail = detail;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    public static void savingCar(int modelId, int brandId, int detailId, int userId) {
        Car car = new Car();
        car.setBrand(Brand.getBrandById(brandId));
        car.setDetail(Detail.getDetailById(detailId));
        car.setModel(Model.getModelById(modelId));
        car.setUser(User.getUserById(userId));

        Session session = HibernateUtil.createSessionFactory().openSession();

        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.save(car);
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(ex);
        } finally {
            session.close();
        }
    }

    public static List getCarByUserId(int userId) {
        List<Car> cars;
        Session session = HibernateUtil.createSessionFactory().openSession();
        Query query = session.createQuery("from Car where user_id = :userId");
        query.setParameter("userId", userId);
        try {
            cars = query.list();
        } catch (Exception ex) {
            return null;
        }
        session.close();
        return cars;
    }

    public static List getAllCars() {
        List<Car> cars;
        Session session = HibernateUtil.createSessionFactory().openSession();
        Query query = session.createQuery("from Car");
        cars = query.list();
        session.close();
        return cars;
    }

    public static List getAllCars(int brandId, int modelId, int minPrice, int maxPrice, int cm3) {
        Session session = HibernateUtil.createSessionFactory().openSession();
        String where = "";
        boolean firstParam = true;
        boolean secondParam = true;
        boolean thirdParam = true;
        boolean fourthParam = true;
        if (brandId != 0) {
            where += "brand_id = " + brandId;
            firstParam = false;
        }
        if (modelId != 0) {
            if (firstParam) {
                where += "model_id = " + modelId;
                secondParam = false;
            } else {
                where += " and model_id = " + modelId;
                secondParam = false;
            }
        }
        if (minPrice != 0) {
            if (secondParam) {
                where += "detail.price >= " + minPrice;
                thirdParam = false;
            } else {
                where += " and detail.price >= " + minPrice;
                thirdParam = false;
            }
        }
        if (maxPrice != 0) {
            if (firstParam && secondParam && thirdParam) {
                where += "detail.price <= " + maxPrice;
                fourthParam = false;
            } else {
                where += "and detail.price <= " + maxPrice;
                fourthParam = false;
            }
        }
        if (cm3 != 0) {
            if (firstParam && secondParam && thirdParam && fourthParam) {
                where += "detail.cubic_capacity = " + cm3;
            }

            if (thirdParam) {
                where += "and detail.cubic_capacity = " + cm3;
            }
        }

        Query query = session.createQuery("from Car where " + where);

        List<Car> cars = query.list();
        session.close();
        return cars;

    }

    public static boolean checkCarAndUser(int carId, int userId) {
        Session session = HibernateUtil.createSessionFactory().openSession();
        Query query = session.createQuery("from Car where id = :carId and user_id = :userId");
        query.setParameter("carId", carId);
        query.setParameter("userId", userId);
        List list = query.list();

        return list.size() == 1;
    }

    public static List getCarById(int carId) {
        Session session = HibernateUtil.createSessionFactory().openSession();
        Query query = session.createQuery("from Car where id = :carId");
        query.setParameter("carId", carId);
        List<Car> cars = query.list();
        session.close();
        return cars;
    }

    public static void UpdatingCar(int carId, int brandId, int modelId, int detailId) {
        Session session = HibernateUtil.createSessionFactory().openSession();
        Query query = session.createQuery("update Car set brand_id = :brandId, model_id = :modelId, detail_id = :detailId where id = :carId");
        query.setParameter("carId", carId);
        query.setParameter("brandId", brandId);
        query.setParameter("modelId", modelId);
        query.setParameter("detailId", detailId);

        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            query.executeUpdate();
            tx.commit();
        } catch (Exception ex) {

        }
        session.close();
    }

    public static void DeleteCarByDetailId(int carId) {
        Session session = HibernateUtil.createSessionFactory().openSession();
        Query query = session.createQuery("delete from Car where id = :carId");
        query.setParameter("carId", carId);

        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            query.executeUpdate();
            tx.commit();
        } catch (Exception ex) {

        }
        session.close();
    }
}
