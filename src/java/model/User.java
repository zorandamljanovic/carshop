package model;

import servlets.HibernateUtil;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

@Entity
@Table(name = "users")
public class User {

    // tess
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "email")
    private String email;
    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;

    @OneToMany(targetEntity = Car.class)
    @JoinColumn(name = "user_id")
    private List<Car> car;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the car
     */
    public List<Car> getCar() {
        return car;
    }

    /**
     * @param car the car to set
     */
    public void setCar(List<Car> car) {
        this.car = car;
    }

    public static boolean saveUser(String name, String email, String username, String password) {
        boolean response = false;
        if (isRegularEmail(email)) {
            password = generatePassword(password);

            User user = new User();
            user.name = name;
            user.email = email;
            user.username = username;
            user.password = password;

            Session session = HibernateUtil.createSessionFactory().openSession();

            Transaction tx = null;

            try {
                tx = session.beginTransaction();
                session.save(user);
                tx.commit();
                response = true;
            } catch (HibernateException ex) {
                if (tx != null) {
                    tx.rollback();
                }
                System.out.println(ex);
            } finally {
                session.close();
            }
        }
        return response;
    }

    public static User getUser(String username, String password) {
        User user = new User();
        password = generatePassword(password);

        Session session = HibernateUtil.createSessionFactory().openSession();
        Query query = session.createQuery("from User user where user.username ='" + username + "' and user.password ='" + password + "'");
        try {
            user = (User) query.list().get(0);
        } catch (Exception e) {
            return null;
        }
        return user;
    }

    public static String generatePassword(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());
            byte[] passwordBytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < passwordBytes.length; i++) {
                sb.append(Integer.toString((passwordBytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            password = sb.toString();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        return password;
    }

    public static boolean isRegularEmail(String email) {
        String pattern = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(email);
        boolean matchFound = m.matches();
        return matchFound;
    }

    public static boolean uniqEmail(String email) {
        Session session = HibernateUtil.createSessionFactory().openSession();
        Query query = session.createQuery("from User user where user.email = '" + email + "'");
        List list = query.list();
        if (list.size() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean uniqUsername(String username) {
        Session session = HibernateUtil.createSessionFactory().openSession();
        Query query = session.createQuery("from User user where user.username = '" + username + "'");
        List list = query.list();
        if(list.size() > 0){
            return false;
        }else{
            return true;
        }
    }
    
    public static User getUserById(int id){
        User user = new User();
        Session session = HibernateUtil.createSessionFactory().openSession();
        Query query = session.createQuery("from User user where user.id = :id");
        query.setParameter("id", id);
        try{
            user = (User)query.list().get(0);
        }catch(Exception e){
            return null;
        }
        session.close();
        return user;
    }
}
