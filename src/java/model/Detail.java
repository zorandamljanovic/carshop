package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import servlets.HibernateUtil;

@Entity
@Table(name = "details")
public class Detail implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "description")
    private String description;
    @Column(name = "mileage")
    private double mileage;
    @Column(name = "cubic_capacity")
    private double cubic_capacity;
    @Column(name = "price")
    private double price;
    @Column(name = "picture_name")
    private String picture_name;
    @Column(name = "phone_number")
    private String phone_number;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the mileage
     */
    public int getMileage() {
        return (int)mileage;
    }

    /**
     * @param mileage the mileage to set
     */
    public void setMileage(double mileage) {
        this.mileage = mileage;
    }

    /**
     * @return the cubic_capacity
     */
    public int getCubic_capacity() {
        return (int)cubic_capacity;
    }

    /**
     * @param cubic_capacity the cubic_capacity to set
     */
    public void setCubic_capacity(double cubic_capacity) {
        this.cubic_capacity = cubic_capacity;
    }

    /**
     * @return the price
     */
    public int getPrice() {
        return (int)price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    public static List getAllDetails() {
        List<Detail> details;
        Session session = HibernateUtil.createSessionFactory().openSession();
        Query query = session.createQuery("from Detail");
        details = query.list();
        return details;
    }

    /**
     * @return the picture_name
     */
    public String getPicture_name() {

        return picture_name;
    }

    /**
     * @param picture_name the picture_name to set
     */
    public void setPicture_name(String picture_name) {
        this.picture_name = picture_name;
    }

    public static void enterPictureName(String fileName) {
        Detail detail = new Detail();
        detail.setPicture_name(fileName);
        Session session = HibernateUtil.createSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.save(detail);
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
    }

    public static boolean uniqPictureName(String imageName) {
        Session session = HibernateUtil.createSessionFactory().openSession();
        Query query = session.createQuery("from Detail d where d.picture_name ='" + imageName + "'");
        List list = query.list();
        if (list.size() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public static void saveAllDetails(String descrptionParam, Double mileageDouble, Double cm3Double, Double priceDouble, String images, String phone) {
        Detail detail = new Detail();
        detail.setDescription(descrptionParam);
        detail.setMileage((double) mileageDouble);
        detail.setCubic_capacity((double) cm3Double);
        detail.setPrice((double) priceDouble);
        detail.setPicture_name(images);
        detail.setPhone_number(phone);

        Session session = HibernateUtil.createSessionFactory().openSession();

        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.save(detail);
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(ex);
        } finally {
            session.close();
        }
    }

    public static List getDetailId(String images) {
        List<Detail> detail;
        Session session = HibernateUtil.createSessionFactory().openSession();
        Query query = session.createQuery("from Detail d where d.picture_name = '" + images + "'");
        detail = query.list();
        return detail;
    }

    public static Detail getDetailById(int id) {
        Detail detail = new Detail();
        Session session = HibernateUtil.createSessionFactory().openSession();
        Query query = session.createQuery("from Detail detail where detail.id = :id");
        query.setParameter("id", id);
        try {
            detail = (Detail) query.list().get(0);
        } catch (Exception ex) {
            return null;
        }
        session.close();
        return detail;
    }

    public String getFirstPictureFromDB() {
        String allPictures = this.getPicture_name();
        String[] arrayPictures = allPictures.split(",");
        String firstPicture = arrayPictures[0];
        return firstPicture;
    }

    public static void updateDetails(int detailId, double cm3, double mileage, double price, String description, String phone) {
        Session session = HibernateUtil.createSessionFactory().openSession();
        Query query = session.createQuery("update Detail as detail set cubic_capacity = :cm3, description = :description, mileage = :mileage, price = :price, phone_number = :phone where id = :detailId");
        query.setParameter("detailId", detailId);
        query.setParameter("cm3", cm3);
        query.setParameter("description", description);
        query.setParameter("mileage", mileage);
        query.setParameter("price", price);
        query.setParameter("phone", phone);
       

        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            query.executeUpdate();
            tx.commit();
        } catch (Exception ex) {

        }
        session.close();
    }
    
    public static void DeleteCarByDetailId(int detailId) {
        Session session = HibernateUtil.createSessionFactory().openSession();
        Query query = session.createQuery("delete from Detail where id = :detailId");
        query.setParameter("detailId", detailId);

        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            query.executeUpdate();
            tx.commit();
        } catch (Exception ex) {

        }
        session.close();
    }
    
    public List getAllPicturesNames(){
        String allPictures = this.getPicture_name();
        String[] arrayPicture = allPictures.split(",");
        List allPicturesNames = new ArrayList();
        for (int i = 0; i < arrayPicture.length; i++) {
            allPicturesNames.add(arrayPicture[i]);
        }
        return allPicturesNames;
    }

    /**
     * @return the phone_number
     */
    public String getPhone_number() {
        return phone_number;
    }

    /**
     * @param phone_number the phone_number to set
     */
    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }
}
