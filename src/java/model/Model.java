package model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.Query;
import org.hibernate.Session;
import servlets.HibernateUtil;

@Entity
@Table(name = "models")
public class Model {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "name")
    private String name;
    @ManyToOne(targetEntity = Brand.class)
    @JoinColumn(name = "brand_id", referencedColumnName = "id")
    private Brand brand;

    /**
     * @return the id
     */
    
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the brand
     */
    public Brand getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public static List getAllModels(int id){
        List<Model>models;
        Session session = HibernateUtil.createSessionFactory().openSession();
        Query query = session.createQuery("from Model where brand_id = " + id);
        models = query.list();
        return models;
    }
    
    public static List getAllModels(){
        List<Model>models;
        Session session = HibernateUtil.createSessionFactory().openSession();
        Query query = session.createQuery("from Model");
        models = query.list();
        return models;
    }
    
    public static String getModelName(String modelParam){
        String modelName = "";
        int id = Integer.parseInt(modelParam);
        List<Model> models = Model.getAllModels(id);
        for(Model model : models){
            modelName = model.getName();
        }
        return modelName;
    }
    
    public static Model getModelById(int id){
        Model model = new Model();
        Session session = HibernateUtil.createSessionFactory().openSession();
        Query query = session.createQuery("from Model model where model.id = :id");
        query.setParameter("id", id);
        try{
            model = (Model)query.list().get(0);
        }catch(Exception e){
            return null;
        }
        session.close();
        return model;
    }
}
