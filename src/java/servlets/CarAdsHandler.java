/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.util.List;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import model.Car;

/**
 *
 * @author zdy
 */
public class CarAdsHandler extends SimpleTagSupport {
    
    private List<Car> cars;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     *
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public void doTag() throws JspException {
        JspWriter out = getJspContext().getOut();
        
        try {
            // TODO: insert code to write html before writing the body content.
            // e.g.:
            //
            // out.println("<strong>" + attribute_1 + "</strong>");
            // out.println("    <blockquote>");
            if (cars == null) {
                out.print("<div class='CarAdsHandler'>");
                out.print("Something went wrong, please follow this <a href = 'result.jsp'>Link</a>");
                out.print("</div>");
            } else {
                if (cars.size() > 0) {
                    out.print("<div class='myAd'>");
                    for (Car car : cars) {
                        out.print("<div class='container'>");
                        out.print("<div class='myAdImage'><a href = 'editAd?carId=" + car.getId() + "&userId=" + car.getUser().getId() + "'><img src='images/" + car.getBrand().getName() + "/" + car.getDetail().getFirstPictureFromDB() + "'height = 150 width = 150></a></div>");
                        out.print("<div class='myAdBrandModel'>" + car.getBrand().getName() + " " + car.getModel().getName() + "</div>");
                        out.print("</div>");
                    }
                    out.print("</div>");
                } else {
                    out.print("<div class='CarAdsHandler'>");
                    out.print("You do not have any ad");
                    out.print("</div>");
                }
                JspFragment f = getJspBody();
                if (f != null) {
                    f.invoke(out);
                }
            }
            // TODO: insert code to write html after writing the body content.
            // e.g.:
            //
            // out.println("    </blockquote>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in CarAdsHandler tag", ex);
        }
    }

    /**
     * @return the cars
     */
    public List<Car> getCars() {
        return cars;
    }

    /**
     * @param cars the cars to set
     */
    public void setCars(List<Car> cars) {
        this.cars = cars;
    }
    
}
