/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import com.sun.webkit.BackForwardList;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Brand;
import model.Car;
import model.Detail;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author zdy
 */
@WebServlet(name = "SavingEditedAd", urlPatterns = {"/savingEditedAd"})
public class SavingEditedAd extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws org.apache.commons.fileupload.FileUploadException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, FileUploadException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String pathFolder = "";
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
        ServletFileUpload upload = new ServletFileUpload(factory);

        String oldBrandId = request.getParameter("brandInput");
        String oldBrandName = Brand.getBrandName(oldBrandId);

        String brandParam = request.getParameter("brand");
        String brandName = Brand.getBrandName(brandParam);
        pathFolder = brandName;
        int brandId = Integer.parseInt(brandParam);

        String modelParam = request.getParameter("model");
        int modelId = Integer.parseInt(modelParam);

        String cm3Param = request.getParameter("cm3");
        double cm3 = Double.parseDouble(cm3Param);

        String mileageParam = request.getParameter("mileage");
        double mileage = Double.parseDouble(mileageParam);

        String priceParam = request.getParameter("price");
        double price = Double.parseDouble(priceParam);

        String description = request.getParameter("description");
        
        String phone = request.getParameter("phone");

        String detailIdParam = request.getParameter("detailIdParam");
        int detailId = Integer.parseInt(detailIdParam);

        String carIdParam = request.getParameter("carIdParam");
        int carId = Integer.parseInt(carIdParam);

        String picturesNames = request.getParameter("picturesNames");
        String[] arrayPictures = picturesNames.split(",");

        if (!oldBrandName.equals(brandName)) {
            String uploadPath = "C:\\Users\\zdy\\Desktop\\NetBeans projects\\online-shop\\web\\images\\";
            String uploadPathWithFolderName = uploadPath + pathFolder;
            File uploadDir = new File(uploadPathWithFolderName);
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }
            for (int i = 0; i < arrayPictures.length; i++) {
                try {
                    Path source = Paths.get(uploadPath + oldBrandName + File.separator + arrayPictures[i]);
                    Path dest = Paths.get(uploadPath + brandName + File.separator + arrayPictures[i]);
                    Files.move(source, dest);
                } catch (Exception ex) {
                    request.setAttribute("message", "There was an error: " + ex.getMessage());
                    request.getRequestDispatcher("/myAd.jsp").forward(request, response);
                }
            }
        }
            Detail.updateDetails(detailId, cm3, mileage, price, description, phone);
            Car.UpdatingCar(carId, brandId, modelId, detailId);
            request.getRequestDispatcher("/unos.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (FileUploadException ex) {
            Logger.getLogger(SavingEditedAd.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (FileUploadException ex) {
            Logger.getLogger(SavingEditedAd.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
