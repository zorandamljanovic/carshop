/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import model.Detail;

/**
 *
 * @author zdy
 */
public class CarPicturesHandler extends SimpleTagSupport {

    private String brandName;
    private List allPictures;

    /**
     * Called by the container to invoke this tag. The implementation of this method is provided by the tag library
     * developer, and handles all tag processing, body iteration, etc.
     */
    @Override
    public void doTag() throws JspException {
        JspWriter out = getJspContext().getOut();

        try {
            // TODO: insert code to write html before writing the body content.
            // e.g.:
            //
            // out.println("<strong>" + attribute_1 + "</strong>");
            // out.println("    <blockquote>");
            for (int i = 0; i < allPictures.size(); i++) {
                out.print("<a href = 'images/" + brandName + "/" + allPictures.get(i) + "' data-lightbox = 'mygallery'><img src = 'images/" + brandName + "/" + allPictures.get(i) + "'></a>");
            }
            JspFragment f = getJspBody();
            if (f != null) {
                f.invoke(out);
            }

            // TODO: insert code to write html after writing the body content.
            // e.g.:
            //
            // out.println("    </blockquote>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in CarPicturesHandler tag", ex);
        }
    }

    /**
     * @return the brandName
     */
    public String getBrandName() {
        return brandName;
    }

    /**
     * @param brandName the brandName to set
     */
    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    /**
     * @return the allPictures
     */
    public List getAllPictures() {
        return allPictures;
    }

    /**
     * @param allPictures the allPictures to set
     */
    public void setAllPictures(List allPictures) {
        this.allPictures = allPictures;
    }

}
