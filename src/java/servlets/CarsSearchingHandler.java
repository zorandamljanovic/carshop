/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.util.List;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import model.Car;

/**
 *
 * @author zdy
 */
public class CarsSearchingHandler extends SimpleTagSupport {

    private List<Car> cars;

    /**
     * Called by the container to invoke this tag. The implementation of this method is provided by the tag library
     * developer, and handles all tag processing, body iteration, etc.
     *
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public void doTag() throws JspException {
        JspWriter out = getJspContext().getOut();

        try {
            // TODO: insert code to write html before writing the body content.
            // e.g.:
            //
            // out.println("<strong>" + attribute_1 + "</strong>");
            // out.println("    <blockquote>");
            if (cars == null) {
                out.print("You must back on Home page");
            } else {
                if (cars.size() > 0) {
                    out.print("<table>");
                    for (Car car : cars) {
                        out.print("<tr>");
                        out.print("<td><a href = 'showAd?carId=" + car.getId() + "'><img src='images/" + car.getBrand().getName() + "/" + car.getDetail().getFirstPictureFromDB() + "'alt = '" + car.getBrand().getName() + "" + car.getModel().getName() + "'height = 150 width = 150></a></td>");
                        out.print("<td>" + car.getBrand().getName() + "&nbsp" + car.getModel().getName() + "<br>");
                        out.print("cm3:&nbsp" + car.getDetail().getCubic_capacity() + "&nbspcm3, Mileage:&nbsp" + car.getDetail().getMileage() + "&nbspkm</td>");
                        out.print("<th><div>" + car.getDetail().getPrice() + "&nbsp€</div></th>");
                        out.print("</tr>");
                    }
                    out.print("</table>");
                } else {
                    out.print("You must back on Home page");
                }
            }
            JspFragment f = getJspBody();
            if (f != null) {
                f.invoke(out);
            }

            // TODO: insert code to write html after writing the body content.
            // e.g.:
            //
            // out.println("    </blockquote>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in CarsSearchingHandler tag", ex);
        }
    }

    /**
     * @return the cars
     */
    public List<Car> getCars() {
        return cars;
    }

    /**
     * @param cars the cars to set
     */
    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

}
