/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import model.Brand;

/**
 *
 * @author zdy
 */
public class BrandsHandler extends SimpleTagSupport {

    private List<Brand> brands;

    /**
     * Called by the container to invoke this tag. The implementation of this
     * method is provided by the tag library developer, and handles all tag
     * processing, body iteration, etc.
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public void doTag() throws JspException {
        JspWriter out = getJspContext().getOut();

        try {
            // TODO: insert code to write html before writing the body content.
            // e.g.:
            //
            // out.println("<strong>" + attribute_1 + "</strong>");
            // out.println("    <blockquote>");
            out.print("<select name='brand' id='brand'>");
            out.print("<option disabled selected value>Brand</option>");
            for (Brand brand : brands) {
                out.print("<option value = '"+brand.getId()+"'>"+brand.getName()+"</option>");
            }
            out.print("</select>");
            JspFragment f = getJspBody();
            if (f != null) {
                f.invoke(out);
            }

            // TODO: insert code to write html after writing the body content.
            // e.g.:
            //
            // out.println("    </blockquote>");
        } catch (java.io.IOException ex) {
            throw new JspException("Error in TagHandlerBrands tag", ex);
        }
    }

    /**
     * @return the brands
     */
    public List<Brand> getBrands() {
        return brands;
    }

    /**
     * @param brands the brands to set
     */
    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }

}
