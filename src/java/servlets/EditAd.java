/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Brand;
import model.Car;

/**
 *
 * @author zdy
 */
@WebServlet(name = "EditAd", urlPatterns = {"/editAd"})
public class EditAd extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            String carIdParam = request.getParameter("carId");
            String userIdParam = request.getParameter("userId");
            int userId = Integer.parseInt(userIdParam);
            int carId = Integer.parseInt(carIdParam);
            
            boolean checkedData = Car.checkCarAndUser(carId, userId);
            if(checkedData){
                List<Car> cars = Car.getCarById(carId);
                int brand = cars.get(0).getBrand().getId();
                List<Brand>brands = Brand.getBrandName(brand);
                String brandName = brands.get(0).getName();
                int model = cars.get(0).getModel().getId();
                int detailId = cars.get(0).getDetail().getId();
                int cm3 = cars.get(0).getDetail().getCubic_capacity();
                int mileage = cars.get(0).getDetail().getMileage();
                int price = cars.get(0).getDetail().getPrice();
                String pictures = cars.get(0).getDetail().getPicture_name();
                String phone = cars.get(0).getDetail().getPhone_number();
                List allPictures = cars.get(0).getDetail().getAllPicturesNames();
                String description = cars.get(0).getDetail().getDescription();
                request.setAttribute("brandName", brandName);
                request.setAttribute("allPictures", allPictures);
                request.setAttribute("pictures", pictures);
                request.setAttribute("detailId", detailId);
                request.setAttribute("carId", carId);
                request.setAttribute("brand", brand);
                request.setAttribute("model", model);
                request.setAttribute("cm3", cm3);
                request.setAttribute("mileage", mileage);
                request.setAttribute("price", price);
                request.setAttribute("phone", phone);
                request.setAttribute("description", description);
                request.getRequestDispatcher("editAd.jsp").forward(request, response);
            }else{
                String message = "Something went wrong";
                request.setAttribute("message",message);
                request.getRequestDispatcher("myAd.jsp").forward(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
