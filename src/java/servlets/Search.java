/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Car;

/**
 *
 * @author zdy
 */
@WebServlet(name = "Search", urlPatterns = {"/search"})
public class Search extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            int brandId = 0;
            int modelId = 0;
            int cm3 = 0;
            int minPrice = 0;
            int maxPrice = 0;
            String message = "";

            String brandParam = request.getParameter("brand");
            if (brandParam != null) {
                brandId = Integer.parseInt(brandParam);
            }
            String modelParam = request.getParameter("model");
            if (modelParam != null) {
                modelId = Integer.parseInt(modelParam);
            }
            String cm3Param = request.getParameter("selected_cm3");
            if (cm3Param != null) {
                cm3 = Integer.parseInt(cm3Param);
            }
            String minPriceParam = request.getParameter("minPrice");
            if (!minPriceParam.equals("")) {
                minPrice = Integer.parseInt(minPriceParam);
            }
            String maxPriceParam = request.getParameter("maxPrice");
            if(!maxPriceParam.equals("")){
                maxPrice = Integer.parseInt(maxPriceParam);
            }

            if (brandParam == null && modelParam == null && cm3Param == null && minPriceParam.equals("") && maxPriceParam.equals("")) {
                List<Car> cars = Car.getAllCars();
                request.setAttribute("cars", cars);
                request.getRequestDispatcher("/search.jsp").forward(request, response);
            } else {

                List<Car> cars = Car.getAllCars(brandId, modelId, minPrice, maxPrice, cm3);
                if (cars.isEmpty()) {
                    message = "there is no car with this parameters!!!";
                }

                request.setAttribute("message", message);
                request.setAttribute("cars", cars);
                request.getRequestDispatcher("/search.jsp").forward(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
