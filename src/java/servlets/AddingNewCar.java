/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Brand;
import model.Car;
import model.Detail;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author zdy
 */
@WebServlet(name = "AddingNewCar", urlPatterns = {"/AddingNewCar"})
@MultipartConfig
public class AddingNewCar extends HttpServlet {

    private static final String UPLOAD_DIRECTORY = "upload";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        Random random = new Random();
        String brandParam = "";
        String pathFolderName = "";
        int brandId = 0;
        int modelId = 0;
        int detailId = 0;
        String cm3Param = "";
        String mileageParam = "";
        String priceParam = "";
        String descriptionParam = "";
        Double cm3Double = null;
        Double mileageDouble = null;
        Double priceDouble = null;
        String userIdParam = "";
        String imageName = "";
        String images = "";
        String phone = "";
        int userId = 0;

        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
        ServletFileUpload upload = new ServletFileUpload(factory);

        ServletContext context = getServletContext();
        String imagesPath = context.getRealPath("");
        String[] imagesPathSplit = imagesPath.split("build");
        String part1 = imagesPathSplit[0];
        imagesPath = part1 + "web\\images";
        String fullPath = "";

        try {
            List formItems = upload.parseRequest(request);
            Iterator iter = formItems.iterator();

            while (iter.hasNext()) {
                String uploadPath = imagesPath + File.separator + pathFolderName;
                File uploadDir = new File(uploadPath);
                if (!uploadDir.exists()) {
                    uploadDir.mkdir();
                }
                FileItem item = (FileItem) iter.next();
                if (!item.isFormField()) {
                    imageName = new File(item.getName()).getName();
                    if (!imageName.equals("")) {
                        if (Detail.uniqPictureName(imageName) != false) {
                            imageName = String.valueOf(random.nextInt(10000)) + imageName;
                            images = images + imageName + ",";
                            String filePath = uploadPath + File.separator + imageName;
                            File storeFile = new File(filePath);
                            fullPath = filePath;
                            item.write(storeFile);
                        } else {
                            imageName = String.valueOf(random.nextInt(10000)) + imageName;
                            images = images + imageName + ",";
                            String filePath = uploadPath + File.separator + imageName;
                            File storeFile = new File(filePath);
                            fullPath = filePath;
                            item.write(storeFile);
                        }
                    }
                } else {
                    String requestParam = item.getFieldName();
                    switch (requestParam) {
                        case "brand":
                            brandParam = item.getString();
                            pathFolderName = Brand.getBrandName(brandParam);
                            brandId = Integer.parseInt(brandParam);
                            break;
                        case "model":
                            modelId = Integer.parseInt(item.getString());
                            break;
                        case "cm3":
                            cm3Param = item.getString();
                            cm3Double = Double.parseDouble(cm3Param);
                            break;
                        case "mileage":
                            mileageParam = item.getString();
                            mileageDouble = Double.parseDouble(mileageParam);
                            break;
                        case "price":
                            priceParam = item.getString();
                            priceDouble = Double.parseDouble(priceParam);
                            break;
                        case "description":
                            descriptionParam = item.getString();
                            break;
                        case "userId":
                            userIdParam = item.getString();
                            userId = Integer.parseInt(userIdParam);
                            break;
                        case "phone":
                            phone = item.getString();
                    }
                }
            }
            Detail.saveAllDetails(descriptionParam, mileageDouble, cm3Double, priceDouble, images, phone);
            List<Detail> details = Detail.getDetailId(images);
//            for(Detail detail : details){
//                detailId = detail.getId();
//            }
            detailId = details.get(0).getId();
            Car.savingCar(modelId, brandId, detailId, userId);
            request.setAttribute("message", "Upload has been done successfully");
            request.getRequestDispatcher("/result.jsp").forward(request, response);
        } catch (Exception ex) {
            request.setAttribute("message", "There was an error: " + ex.getMessage());
            request.getRequestDispatcher("/result.jsp").forward(request, response);
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
