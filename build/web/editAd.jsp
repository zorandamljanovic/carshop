<%-- 
    Document   : edit
    Created on : Feb 11, 2020, 10:11:59 PM
    Author     : zdy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="/WEB-INF/tlds/newtag_library.tld" prefix="mt"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css">
        <link rel="stylesheet" type="text/css" href="lightbox.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="javascript/lightbox-plus-jquery.min.js"></script>
        <script src="https://kit.fontawesome.com/f922825c19.js"></script>
        <link rel = "shortcut icon" href = "logo.png" type = "image/x-icon"> 
        <title>Zoran used cars</title>
    </head>
    <body onload="edit.MyBrand(); edit.MyModel(); edit.MyCm3()">
        <%
            if (request.getSession().getAttribute("name") == null) {

        %>
        <nav class="nav-bar">
            <div class="homePage">
                <a href="indexData" title="Home"><i class="fas fa-home"></i></a>
            </div>
            <div class="login-signup">
                <a href="signup.jsp">Signup</a>
                <a href="login.jsp">Login</a>
            </div>
        </nav>
        <%      out.print("This action is not allowed for you");
                return;
            }
        %>
        <%
            if (request.getAttribute("brand") == null) {
        %>
        <nav class="nav-bar">
            <div class="homePage">
                <a href="indexData" title="Home"><i class="fas fa-home"></i></a>
            </div>
            <div class="user-menu">
                <a href="myad?userId=${sessionScope.id}">My Ads</a>
                <a href="add">New Ad</a>
            </div>
            <div class="login-signup">
                <a href="logout">Logout</a>
            </div>
        </nav>

        <%
                out.print("You must back to your ad's");
                return;
            }
        %>
        <div class="nav-div">
            <nav class="nav-bar">
                <div class="homePage">
                    <a href="indexData" title="Home"><i class="fas fa-home"></i></a>
                </div>
                <div class="user-menu">
                    <a href="myad?userId=${sessionScope.id}">My Ads</a>
                    <a href="add">New Ad</a>
                </div>
                <div class="login-signup">
                    <a href="logout">Logout</a>
                </div>
            </nav>
        </div>
        <div class="editAd_div">
            <div class="gallery">
                <mt:CarPicturesHandler  brandName="${brandName}" allPictures="${allPictures}"></mt:CarPicturesHandler>
                </div>
                <form id="formSavingEditedAd" method="POST" action="savingEditedAd">
                    <input id="brandInput" name="brandInput" type="hidden" value="${brand}">
                <input id="modelInput" type="hidden" value="${model}">                      
                <input id="cm3Input" type="hidden" value="${cm3}">
                <input name="carIdParam" type="hidden" value="${carId}">
                <input name="detailIdParam" type="hidden" value="${detailId}">
                <input name="picturesNames" type="hidden" value="${pictures}">
                <input name="phone_number" type="hidden" value="${phone}">

                ${message}
                <div class="editAdDivMenu">
                    Brand
                    <div id="brand_div" onchange="edit.getModels()"></div>
                    Model
                    <div id="model_div"></div>
                    Cubic capacity
                    <div id="cm3_div"></div>
                    Mileage
                    <div id="mileage_div">
                        <input id="mileage" type="text" name="mileage" id="mileage" value="${mileage}">
                    </div>
                    <div class="textRed hidden" id="mileageError">Mileage must be entered</div>
                    Price
                    <div id="price_div">
                        <input id="price" type="text" name="price" value="${price}">
                    </div>
                    <div id="priceError" class="textRed hidden">Price must be entered</div>
                    Phone number
                    <div id="phone_div">
                        <input id="phone" type="text" name="phone" value="${phone}">
                    </div>
                    <div id="phoneError" class="textRed hidden">Phone number must be entered</div>
                </div>
                <div id="description_div">
                    Description
                    <textarea id="desc" name="description">${description}</textarea>
                </div>
                <input class="editAdButtons" type="button" value="Save Changes" onclick="edit.submitForm()">
            </form>
                <form id="deleteButtonForm" method="POST" action="deleteAd">
                <input type="hidden" name="brandId" value="${brand}">
                <input type="hidden" name="detailId" value="${detailId}">
                <input type="hidden" name="pictures" value="${pictures}">
                <input class="editAdButtons" type="submit" value="Delete Ad">
            </form>
        </div>
    </body>
    <script type="text/javascript" src="javascript/edit.js"></script>
</html>
