<%-- 
    Document   : index
    Created on : Nov 11, 2019, 12:30:43 PM
    Author     : zdy
--%>
<%@page import="java.util.List"%>
<%@page import="model.Brand"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="/WEB-INF/tlds/newtag_library.tld" prefix="mt"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://kit.fontawesome.com/f922825c19.js"></script>
        <link rel = "shortcut icon" href = "logo.png" type = "image/x-icon"> 
        <title>Zoran used cars</title>
    </head>
    <body>
        <%
            if (request.getSession().getAttribute("name") != null) {
        %>
        <nav class="nav-bar">
            <div class="user-menu">
                <a href="myad?userId=${sessionScope.id}">My Ads</a>
                <a href="add">New Ad</a>
            </div>
            <div class="login-signup">
                <a href="logout">Logout</a>
            </div>
        </nav>
        <div id="home-search">
            <form method="post" action="search">
                <div id="brand_div" onchange="index.getModels()">
                    <mt:BrandsHandler brands="${brands}"></mt:BrandsHandler>
                    </div>
                    <div id="model_div">
                        <select name="model">
                            <option disabled selected value>Model</option>
                        </select>
                    </div>
                    <div id="cm3">
                        <select name="selected_cm3">
                            <option disabled selected value>cm3</option>
                            <option>500</option>
                            <option>1150</option>
                            <option>1300</option>
                            <option>1600</option>
                            <option>1800</option>
                            <option>2000</option>
                            <option>2500</option>
                            <option>3000</option>
                            <option>3500</option>
                            <option>4500</option>
                        </select>
                    </div>
                    <div>
                        <input id="fprice" name="minPrice" autocomplete="off" type="text" placeholder="min-Price €"/>
                    </div>
                    <div>
                        <input id="fprice" name="maxPrice" autocomplete="off" type="text" placeholder="max-Price €"/>
                    </div>
                    <div>
                        <input class="SearcButton" type="submit" value="Search">
                    </div>
                </form>
            </div>
        <%
        } else {
        %>
        <nav class="nav-bar">
            <div class="login-signup">
                <a href="signup.jsp">Signup</a>
                <a href="login.jsp">Login</a>
            </div>
        </nav>
        <div id="home-search">
            <form method="post" action="search">
                <div id="brand_div" onchange="index.getModels()">
                    <mt:BrandsHandler brands="${brands}"></mt:BrandsHandler>
                    </div>
                    <div id="model_div">
                        <select name="model">
                            <option disabled selected value>Model</option>
                        </select>
                    </div>
                    <div id="cm3">
                        <select name="selected_cm3">
                            <option disabled selected value>cm3</option>
                            <option>500</option>
                            <option>1150</option>
                            <option>1300</option>
                            <option>1600</option>
                            <option>1800</option>
                            <option>2000</option>
                            <option>2500</option>
                            <option>3000</option>
                            <option>3500</option>
                            <option>4500</option>
                        </select>
                    </div>
                    <div>
                        <input id="fprice" name="minPrice" autocomplete="off" type="text" placeholder="min-Price €"/>
                    </div>
                    <div>
                        <input id="fprice" name="maxPrice" autocomplete="off" type="text" placeholder="max-Price €"/>
                    </div>
                    <div>
                        <input class="SearcButton" type="submit" value="Search">
                    </div>
                </form>
            </div>
        <% }%>
        <script type="text/javascript" src="javascript/index.js"></script>
    </body>
</html>
