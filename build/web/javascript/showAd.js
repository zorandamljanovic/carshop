/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

const showAd = {};

showAd.MyBrand = function () {
    let selectedBrand = document.getElementById("brandInput").value;
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "GetBrands");
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            document.getElementById("brand_div").innerHTML = "<select name='brand' id='brand'>" + xmlhttp.responseText + "</select>";
        }
    };
    xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send("selectedBrand=" + selectedBrand);
};

showAd.getModels = function () {
    var element = document.getElementById("brand");
    var id = element.options[element.selectedIndex].value;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "getModels");
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            document.getElementById("model_div").innerHTML = "<select name='model'>" + xmlhttp.responseText + "</select>";
        }
    };
    xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send("id=" + id);
};

showAd.MyModel = function () {
    let selectedModel = document.getElementById("modelInput").value;
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "getModels");
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            document.getElementById("model_div").innerHTML = "<select name='model'>" + xmlhttp.responseText + "</select>";
        }
    };
    xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send("selectedModel=" + selectedModel);
};

showAd.MyCm3 = function () {
    let selectedCm3 = document.getElementById("cm3Input").value;
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "GetDetails");
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            document.getElementById("cm3_div").innerHTML = "<select name='cm3'>" + xmlhttp.responseText + "</select>";
        }
    };
    xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send("selectedCm3=" + selectedCm3);
};

function thumbChange(img){
    let nextPic = img;
    document.getElementById("mainImg").src = nextPic;
}