/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


const index = {};

index.getModels = function () {
    var element = document.getElementById("brand");
    var id = element.options[element.selectedIndex].value;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "getModels");
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            document.getElementById("model_div").innerHTML = "<select name='model'>" + xmlhttp.responseText + "</select>";
        }
    };
    xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send("id=" + id);

};