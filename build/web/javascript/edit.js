/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

const edit = {};

edit.MyBrand = function () {
    let selectedBrand = document.getElementById("brandInput").value;
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "GetBrands");
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            document.getElementById("brand_div").innerHTML = "<select name='brand' id='brand'>" + xmlhttp.responseText + "</select>";
        }
    };
    xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send("selectedBrand=" + selectedBrand);
};

edit.getModels = function () {
    var element = document.getElementById("brand");
    var id = element.options[element.selectedIndex].value;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "getModels");
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            document.getElementById("model_div").innerHTML = "<select name='model'>" + xmlhttp.responseText + "</select>";
        }
    };
    xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send("id=" + id);
};

edit.MyModel = function () {
    let selectedModel = document.getElementById("modelInput").value;
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "getModels");
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            document.getElementById("model_div").innerHTML = "<select name='model'>" + xmlhttp.responseText + "</select>";
        }
    };
    xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send("selectedModel=" + selectedModel);
};

edit.MyCm3 = function () {
    let selectedCm3 = document.getElementById("cm3Input").value;
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "GetDetails");
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            document.getElementById("cm3_div").innerHTML = "<select name='cm3'>" + xmlhttp.responseText + "</select>";
        }
    };
    xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlhttp.send("selectedCm3=" + selectedCm3);
};

edit.submitForm = function () {
    let mileage = document.getElementById("mileage").value;
    if (mileage === "") {
        document.getElementById("mileageError").classList.remove("hidden");
        return false;
    }
    if (isNaN(mileage)) {
        document.getElementById("mileageError").classList.remove("hidden");
        return false;
    }
    let price = document.getElementById("price").value;
    if (price === "") {
        document.getElementById("priceError").classList.remove("hidden");
        return false;
    }
    if (isNaN(price)) {
        document.getElementById("priceError").classList.remove("hidden");
        return false;
    }
    let phone = document.getElementById("phone").value;
    if (phone === "") {
        document.getElementById("phoneError").classList.remove("hidden");
        return false;
    }
    if (isNaN(phone)) {
        alert("for phone you must eneter a number");
        return false;
    }

    document.getElementById("formSavingEditedAd").submit();
};