<%-- 
    Document   : result
    Created on : Dec 16, 2019, 4:26:20 PM
    Author     : zdy
--%>

<%@page import="java.io.File"%>
<%@page import="java.io.BufferedOutputStream"%>
<%@page import="java.io.BufferedInputStream"%>
<%@page import="java.io.FileInputStream"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css">
        <script src="https://kit.fontawesome.com/f922825c19.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel = "shortcut icon" href = "logo.png" type = "image/x-icon"> 
        <title>Zoran used cars</title>
    </head>
    <body>
        <%
            if (request.getSession().getAttribute("name") == null) {
        %>
        <nav class="nav-bar">
            <div class="homePage">
                <a href="indexData" title="Home"><i class="fas fa-home"></i></a>
            </div>
            <div class="login-signup">
                <a href="signup.jsp">Signup</a>
                <a href="login.jsp">Login</a>
            </div>
        </nav>
        <%
                out.print("You are not login");
                return;
            }
        %>

        <nav class="nav-bar">
            <div class="homePage">
                <a href="indexData" title="Home"><i class="fas fa-home"></i></a>
            </div>
            <div class="user-menu">
                <a href="myad?userId=${sessionScope.id}">My Ads</a>
                <a href="add">New ad</a>
            </div>
            <div class="login-signup">
                <a href="logout">Logout</a>
            </div>
        </nav>
        ${message}
    </body>
</html>
