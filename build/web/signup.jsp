<%-- 
    Document   : signup
    Created on : Dec 4, 2019, 10:57:08 PM
    Author     : zdy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css">
        <script src="https://kit.fontawesome.com/f922825c19.js"></script>
        <title>Zoran used cars</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel = "shortcut icon" href = "logo.png" type = "image/x-icon">
    </head>
    <body>

        <%
            if (request.getSession().getAttribute("id") != null) {

        %>
        <nav class="nav-bar">
            <div class="homePage">
                <a href="indexData" title="Home"><i class="fas fa-home"></i></a>
            </div>
            <div class="user-menu">
                <a href="result.jsp">Ads</a>
            </div>
            <div class="login-signup">
                <a href="logout">Logout</a>
            </div>
        </nav>
        <%                out.print("You are already logged");
                return;
            }
        %>
        <nav class="nav-bar">
            <div class="homePage">
                <a href="indexData" title="Home"><i class="fas fa-home"></i></a>
            </div>
            <div class="login-signup">
                <a href="login.jsp">Login</a>
            </div>
        </nav>
        <%
            String name;
            if (request.getAttribute("name") == null) {
                name = "";
            } else {
                name = request.getAttribute("name").toString();
            }
            String email;
            if (request.getAttribute("email") == null) {
                email = "";
            } else {
                email = request.getAttribute("email").toString();
            }
            String username;
            if (request.getAttribute("username") == null) {
                name = "";
            } else {
                name = request.getAttribute("username").toString();
            }
        %>
        <div class="signup-box"><h1>Sign up</h1>
            <form method="post" action="signup">
                <div class="name-div">
                    <input id="fname" type="text" name="name" value="${name}" required="upisi ime" autocomplete="off" placeholder="Name"/><br/>
                </div>
                <c:choose>
                    <c:when test="${errEmail != 'Exists'}">

                        <div class="email-div">
                            <input id="femail" type="text" name="email" onfocusout="signup.checkEmail()" value="${email}" required autocomplete="off" placeholder="E-mail"/><br/>
                        </div>
                        <div id="emailMessage" style="display: none; color: red"></div>
                    </c:when>
                    <c:otherwise>

                        <div class="email-div">
                            <input id="femail" type="text" name="email" onfocusout="signup.checkEmail()" value="${email}" required autocomplete="off" placeholder="E-mail"/><br/>
                        </div>
                        <div id="emailMessage" style="color: red">Choose other email</div>
                    </c:otherwise>
                </c:choose>

                <c:choose>
                    <c:when test="${errUsername != 'Exists'}">
                        <div class="username-div">
                            <i class="fas fa-user"></i>
                            <input id="fusername" type="text" name="username" onfocusout="signup.checkUsername()" value="${username}" required autocomplete="off" placeholder="Username"/><br/>
                        </div>
                        <div id="usernameMessage" style="display: none; color: red"></div>
                    </c:when>
                    <c:otherwise>
                        <div class="username-div">
                            <i class="fas fa-user"></i>
                            <input id="fusername" type="text" name="username" onfocusout="signup.checkUsername()" value="${username}" required autocomplete="off" placeholder="Username"/><br/>
                        </div>
                        <div id="usernameMessage" style="color: red">Enter a new username</div>
                    </c:otherwise>
                </c:choose>

                <div class="password-div">
                    <i class="fas fa-lock"></i>
                    <input type="password" name="password" autocomplete="off" required placeholder="Password"/><br/>
                </div>
                <input class="button" id="signupButton" type="submit" value="Signup"/><br/>
            </form>
        </div>
        <script type="text/javascript" src="javascript/signup.js"></script>
    </body>
</html>
