<%-- 
    Document   : unos
    Created on : Oct 30, 2019, 10:23:57 AM
    Author     : zdy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        <script src="https://kit.fontawesome.com/f922825c19.js"></script>
        <link rel="stylesheet" type="text/css" href="style.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel = "shortcut icon" href = "logo.png" type = "image/x-icon"> 
        <title>Zoran used cars</title>
    </head>
    <body>
        <%
            if (request.getSession().getAttribute("name") == null) {

        %>
        <nav class="nav-bar">
            <div class="homePage">
                <a href="indexData" title="Home"><i class="fas fa-home"></i></a>
            </div>
            <div class="login-signup">
                <a href="signup.jsp">Signup</a>
                <a href="login.jsp">Login</a>
            </div>
        </nav>
        <%        out.print("You are not login");
                return;
            }
        %>
        <nav class="nav-bar">
            <div class="homePage">
                <a href="indexData" title="Home"><i class="fas fa-home"></i></a>
            </div>
            <div class="user-menu">
                <a href="myad?userId=${sessionScope.id}">My Ads</a>
                <a href="add">New Ad</a>
            </div>
            <div class="login-signup">
                <a href="logout">Logout</a>
            </div>
        </nav>
    </body>
</html>
