<%-- 
    Document   : myAdds
    Created on : Feb 9, 2020, 3:27:39 PM
    Author     : zdy
--%>
<%@page import="java.util.List"%>
<%@page import="model.Car"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="/WEB-INF/tlds/newtag_library.tld" prefix="mt"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css"/>
        <script src="https://kit.fontawesome.com/f922825c19.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel = "shortcut icon" href = "logo.png" type = "image/x-icon"> 
        <title>Zoran used cars</title>
    </head>
    <body>
        <%
            if (request.getSession().getAttribute("name") == null) {

        %>
        <nav class="nav-bar">
            <div class="homePage">
                <a href="indexData" title="Home"><i class="fas fa-home"></i></a>
            </div>
            <div class="login-signup">
                <a href="signup.jsp">Signup</a>
                <a href="login.jsp">Login</a>
            </div>
        </nav>
        <%      out.print("This action is not allowed for you");
                return;
            }
        %>
        <div class="nav_div">
            <nav class="nav-bar">
                <div class="homePage">
                    <a href="indexData" title="Home"><i class="fas fa-home"></i></a>
                </div>
                <div class="user-menu">
                    <a href="add">New Ad</a>
                </div>
                <div class="login-signup">
                    <a href="logout">Logout</a>
                </div>
            </nav>
        </div>
        <span style="color: red">${message}</span></br>
        <mt:CarAdsHandler cars="${cars}"></mt:CarAdsHandler>
    </body>
    <script type="text/javascript" src="javascript/myAd.js"></script>
</html>
