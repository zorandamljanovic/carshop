<%-- 
    Document   : oglas
    Created on : Nov 11, 2019, 8:50:50 AM
    Author     : zdy
--%>

<%@page import="java.util.List"%>
<%@page import="model.Brand"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="/WEB-INF/tlds/newtag_library.tld" prefix="mt"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css">
        <script src="https://kit.fontawesome.com/f922825c19.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel = "shortcut icon" href = "logo.png" type = "image/x-icon"> 
        <title>Zoran used cars</title>
    </head>
    <body>
        <%
            if (request.getSession().getAttribute("name") == null) {

        %>  
        <nav class="nav-bar">
            <div class="homePage">
                <a href="indexData" title="Home"><i class="fas fa-home"></i></a>
            </div>
            <div class="login-signup">
                <a href="signup.jsp">Signup</a>
                <a href="login.jsp">Login</a>
            </div>
        </nav>
        <%        out.print("You are not login");
                return;
            }
        %>
        <nav class="nav-bar">
            <div class="homePage">
                <a href="indexData" title="Home"><i class="fas fa-home"></i></a>
            </div>
            <div class="user-menu">
                <a href="result.jsp">Ads</a>
            </div>
            <div class="login-signup">
                <a href="logout">Logout</a>
            </div>
        </nav>
        <form id="formAddNewCar" method="post" action="AddingNewCar" enctype="multipart/form-data">
            <input type="hidden" name="userId" value="${sessionScope.id}">
            <div id="brand_div" onchange="add.getModels()">
                <mt:BrandsHandler brands="${brands}"></mt:BrandsHandler>
            </div>
            <div class="textRed hidden" id="brandError">Brand must be selected</div>
            <div id="model_div">
                <select id="model" name="model">
                    <option disabled selected value>Model</option>
                </select>
            </div>
            <div class="textRed hidden" id="modelError">Model must be selected</div>
            <div>
                <select id="cm3" name="cm3">
                    <option disabled selected value>cm3</option>
                    <option>500</option>
                    <option>1150</option>
                    <option>1300</option>
                    <option>1600</option>
                    <option>1800</option>
                    <option>2000</option>
                    <option>2500</option>
                    <option>3000</option>
                    <option>3500</option>
                    <option>4500</option>
                </select>
            </div>
            <div class="textRed hidden" id="cm3Error">Cubic capacity must be selected</div><br/>
            <div>
                <input id="file" type="file" name="pictures" accept="image/*" multiple/>
                <label for="file">Add Photo</label>
            </div>
            <div>
                <input id="mileage" type="text" name="mileage" autocomplete="off" placeholder="Enter mileage"/>
            </div>
            <div class="textRed hidden" id="mileageError">Mileage must be entered</div><br/>
            <div>
                <input id="price" type="text" name="price" autocomplete="off" placeholder="Price €"/>
            </div>
            <div id="priceError" class="textRed hidden">Price must be entered</div><br/>
            <div>
                Description
                <textarea name="description"></textarea>
            </div>
            <div>
                <input id="phone" name="phone" type="text" placeholder="Phone number"/>
            </div>
            <div id="phoneError" class="textRed hidden">Phone number must be entered</div><br/>
            <input id="addButton" type="button" value="Create Add" onclick="add.submitForm()"/>
        </form>
        <script type="text/javascript" src="javascript/add.js"></script>
    </body>
</html>