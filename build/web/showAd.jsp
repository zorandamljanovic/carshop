<%-- 
    Document   : showAd
    Created on : Mar 31, 2020, 12:48:23 PM
    Author     : zdy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="/WEB-INF/tlds/newtag_library.tld" prefix="mt"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css">
        <link rel="stylesheet" type="text/css" href="lightbox.min.css">
        <script src="javascript/lightbox-plus-jquery.min.js"></script>
        <script src="https://kit.fontawesome.com/f922825c19.js"></script>
        <link rel = "shortcut icon" href = "logo.png" type = "image/x-icon">
        <title>Zoran used cars</title>
    </head>
    <body>
        <%
            if (request.getSession().getAttribute("name") == null) {
        %>
        <nav class="nav-bar">
            <div class="homePage">
                <a href="indexData" title="Home"><i class="fas fa-home"></i></a>
            </div>
            <div class="login-signup">
                <a href="signup.jsp">Signup</a>
                <a href="login.jsp">Login</a>
            </div>
        </nav>
        <%
            if (request.getAttribute("brand") == null) {
                out.print("You didn't choose car to see");
                return;
            } else {
        %>
        <%--
        <div class="gallery">
            <mt:CarPicturesHandler  brandName="${brandName}" allPictures="${allPictures}"></mt:CarPicturesHandler>
            </div>
        --%>
        <div class="Ad">
            <div class="Images">
                <div class="main">
                    <img id="mainImg" src="images/${brandName}/${firstPicture}">
                </div>
                <ul class="thumbs">
                    <c:forTokens items="${allPictures}" delims="," var="name">
                        <li onclick="thumbChange('images/${brandName}/${name}')"><img src="images/${brandName}/<c:out value="${name}"/>"></li>
                        </c:forTokens>
                </ul>
            </div>
            <div class="Description">
                <div id="brand_div">Brand: ${brandName}</div></br>
                <div id="model_div">Model: ${modelName}</div><br/>
                <div id="cm3_div">Cubic capacity: ${cm3}</div></br>
                <div id="mileage_div">Mileage: ${mileage} km</div></br>
                <div id="price_div">Price: ${price}&nbsp€</div></br>
                <div id="phone_div">Phone number: ${phone}</div>
            </div>
            <div id="descTitle" class="description"><h3>Description</h3></div>
            <div id="description_div" class="description">${description}</div>
        </div>

        <%
            }
        } else {
            if (request.getAttribute("brand") == null) {
        %>
        <nav class="nav-bar">
            <div class="homePage">
                <a href="indexData" title="Home"><i class="fas fa-home"></i></a>
            </div>
            <div class="login-signup">
                <a href="logout">Logout</a>
            </div>
        </nav>
        <%
                out.print("You didn't choose car to see");
                return;
            }
        %>
        <nav class="nav-bar">
            <div class="homePage">
                <a href="indexData" title="Home"><i class="fas fa-home"></i></a>
            </div>
            <div class="login-signup">
                <a href="logout">Logout</a>
            </div>
        </nav>
        <%--
        <div class="gallery">
            <mt:CarPicturesHandler  brandName="${brandName}" allPictures="${allPictures}"></mt:CarPicturesHandler>
            </div>
        --%>
        <div class="Ad">
            <div class="Images">
                <div class="main">
                    <a href="images/${brandName}/${firstPicture}" data-lightbox = "mygallery"><img id="mainImg" src="images/${brandName}/${firstPicture}"></a>
                </div>
                <ul class="thumbs">
                    <c:forTokens items="${allPictures}" delims="," var="name">
                        <li onclick="thumbChange('images/${brandName}/${name}')"><img src="images/${brandName}/<c:out value="${name}"/>"></li>
                        </c:forTokens>
                </ul>
            </div>
            <div class="Description">
                <div id="brand_div">Brand: ${brandName}</div></br>
                <div id="model_div">Model: ${modelName}</div><br/>
                <div id="cm3_div">Cubic capacity: ${cm3}</div></br>
                <div id="mileage_div">Mileage: ${mileage} km</div></br>
                <div id="price_div">Price: ${price}&nbsp€</div></br>
                <div id="phone_div">Phone number: ${phone}</div>
            </div>
            <div id="descTitle" class="description"><h3>Description</h3></div>
            <div id="description_div" class="description">${description}</div>
        </div>
        <% }%>
        <script type="text/javascript" src="javascript/showAd.js"></script>
    </body>
</html>
