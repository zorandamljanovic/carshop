CREATE DATABASE  IF NOT EXISTS `onlineshop` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `onlineshop`;
-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: onlineshop
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES (1,'Ford'),(2,'Zastava'),(3,'AC'),(4,'Alfa Romeo'),(5,'Aro'),(6,'Aston Martin'),(7,'Audi'),(8,'Austin'),(9,'Bentley'),(10,'BMW'),(11,'Chevrolet'),(12,'Citroen'),(13,'Dacia'),(14,'Fiat'),(15,'Honda'),(16,'Kia'),(17,'Lada'),(18,'Mazda'),(19,'Mercedes Benz'),(20,'Opel'),(21,'Peugeot'),(22,'Renault'),(23,'Seat'),(24,'Saab'),(25,'Smart'),(26,'Subaru'),(27,'Suzuki'),(28,'Skoda'),(29,'Toyota'),(30,'Volkswagen'),(31,'Volvo');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cars`
--

DROP TABLE IF EXISTS `cars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) NOT NULL,
  `detail_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `brand_id` (`brand_id`),
  KEY `detail_id` (`detail_id`),
  KEY `model_id` (`model_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `cars_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`),
  CONSTRAINT `cars_ibfk_2` FOREIGN KEY (`detail_id`) REFERENCES `details` (`id`),
  CONSTRAINT `cars_ibfk_3` FOREIGN KEY (`model_id`) REFERENCES `models` (`id`),
  CONSTRAINT `cars_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cars`
--

LOCK TABLES `cars` WRITE;
/*!40000 ALTER TABLE `cars` DISABLE KEYS */;
INSERT INTO `cars` VALUES (20,14,288,145,30),(23,1,291,16,40);
/*!40000 ALTER TABLE `cars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `details`
--

DROP TABLE IF EXISTS `details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cubic_capacity` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `mileage` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `picture_name` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=292 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `details`
--

LOCK TABLES `details` WRITE;
/*!40000 ALTER TABLE `details` DISABLE KEYS */;
INSERT INTO `details` VALUES (288,2000,'',154200,150000,'5614_TLI9554.jpg,81202.jpg,1854a.jpg,'),(291,2000,'fabrika',0,17999,'83332141371957-ford-kuga-cockpit-modell-2016-suv-cab.jpg,2026be24d9e2496de6e6ae45960de3ae19d9.png,729Ford-Kuga-Facelift-2016-1.jpg,1996unnamed.jpg,');
/*!40000 ALTER TABLE `details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `models`
--

DROP TABLE IF EXISTS `models`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `models` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `brand_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `brand_id` (`brand_id`),
  CONSTRAINT `models_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=368 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `models`
--

LOCK TABLES `models` WRITE;
/*!40000 ALTER TABLE `models` DISABLE KEYS */;
INSERT INTO `models` VALUES (1,'Focus',1),(2,'Yugo',2),(4,'Aerostar',1),(5,'B-Max',1),(6,'C-Max',1),(7,'Capri',1),(8,'Cortina',1),(9,'Cougar',1),(10,'Courier',1),(11,'Escort',1),(12,'Fiesta',1),(13,'Fusion',1),(14,'Galaxy',1),(15,'Ka',1),(16,'Kuga',1),(17,'Mondeo',1),(18,'S-Max',1),(19,'101',2),(20,'128',2),(21,'750',2),(22,'850',2),(23,'Florida',2),(24,'Koral',2),(25,'Koral in',2),(26,'Poly',2),(27,'Skala 55',2),(28,'Yugo 45',2),(29,'Yugo 55',2),(30,'Yugo 60',2),(31,'Yugo 65',2),(32,'Yugo Cabrio',2),(33,'Yugo Tempo',2),(34,'Cobra',3),(35,'145',4),(36,'146',4),(37,'147',4),(38,'155',4),(39,'156',4),(40,'159',4),(41,'164',4),(42,'166',4),(43,'Brera',4),(44,'Giulia',4),(45,'Giulietta',4),(46,'GT',4),(47,'MiTo',4),(48,'Serija 10',5),(49,'Rapide',6),(50,'80',7),(51,'90',7),(52,'100',7),(53,'A1',7),(54,'A2',7),(55,'A3',7),(56,'A4',7),(57,'A5',7),(58,'A6',7),(59,'A7',7),(60,'A8',7),(61,'Coupe',7),(62,'Q2',7),(63,'Q3',7),(64,'Q5',7),(65,'Q7',7),(66,'Q8',7),(67,'R8',7),(68,'RS6',7),(69,'S3',7),(70,'S4',7),(71,'S5',7),(72,'S6',7),(73,'S8',7),(74,'TT',7),(75,'Mini',8),(76,'Moris Marina',8),(77,'Bentyaga',9),(78,'Continental',9),(79,'Series 1',10),(80,'114',10),(81,'116',10),(82,'118',10),(83,'120',10),(84,'123',10),(85,'125',10),(86,'Series 2',10),(87,'216',10),(88,'218',10),(89,'220',10),(90,'Series 3',10),(91,'315',10),(92,'316',10),(93,'318',10),(94,'320',10),(95,'323',10),(96,'324',10),(97,'325',10),(98,'328',10),(99,'330',10),(100,'335',10),(101,'335',10),(102,'Astro',11),(103,'Aveo',11),(104,'Blazer',11),(105,'Camaro',11),(106,'Caprice',11),(107,'Captiva',11),(108,'Cruze',11),(109,'Epica',11),(110,'Evanda',11),(111,'Impala',11),(112,'Kalos',11),(113,'Lacetti',11),(114,'Lumina',11),(115,'Malibu',11),(116,'Matiz',11),(117,'Nubira',11),(118,'Orlando',11),(119,'Spark',11),(120,'Tacuma',11),(121,'Trax',11),(122,'Berlingo',12),(123,'BX',12),(124,'C1',12),(125,'C2',12),(126,'C3',12),(127,'C3 Picasso',12),(128,'C4',12),(129,'C4 Cactus',12),(130,'C4 Grand Picasso',12),(131,'C4 Picasso',12),(132,'C5',12),(133,'C6',12),(134,'C8',12),(135,'Saxo',12),(136,'Xantia',12),(137,'Xsara',12),(138,'ZX',12),(139,'Double',13),(140,'Duster',13),(141,'Logan',13),(142,'Logan',13),(143,'Sandero',13),(144,'Stepway',13),(145,'125',14),(146,'126',14),(147,'127',14),(148,'500',14),(149,'500C',14),(150,'500L',14),(151,'500X',14),(152,'850',14),(153,'Albea',14),(154,'Brava',14),(155,'Bravo',14),(156,'Croma',14),(157,'Doblo',14),(158,'EVO',14),(159,'Grande Punto',14),(160,'Linea',14),(161,'Marea',14),(162,'Multipla',14),(163,'Panda',14),(164,'Punto',14),(165,'Stilo',14),(166,'Tipo',14),(167,'Uno',14),(168,'Accord',15),(169,'Civic',15),(170,'CR-V',15),(171,'CRX',15),(172,'HR-V',15),(173,'Jazz',15),(174,'Legend',15),(175,'Rio',16),(176,'Sportage',16),(177,'Sorento',16),(178,'Soul',16),(179,'Venga',16),(180,'Aleko',17),(181,'Niva',17),(182,'Riva',17),(183,'Samara',17),(184,'2',18),(185,'3',18),(186,'6',18),(187,'323',18),(188,'626',18),(189,'CX-3',18),(190,'CX-5',18),(191,'CX-7',18),(192,'CX-30',18),(193,'A Class',19),(194,'B Class',19),(195,'C Class',19),(196,'CLA Class',19),(197,'CL Class',19),(198,'E Class',19),(199,'G Class',19),(200,'GL Class',19),(201,'ML Class',19),(202,'R Class',19),(203,'S Class',19),(204,'Adam',20),(205,'Agila',20),(206,'Antara',20),(207,'Ascona',20),(208,'Astra',20),(209,'Calibra',20),(210,'Corsa',20),(211,'Frontera',20),(212,'Insignia',20),(213,'Manta',20),(214,'Meriva',20),(215,'Omega',20),(216,'Tigra',20),(217,'Vectra',20),(218,'Zafira',20),(219,'104',21),(220,'106',21),(221,'107',21),(222,'108',21),(223,'204',21),(224,'205',21),(225,'206',21),(226,'207',21),(227,'208',21),(228,'305',21),(229,'306',21),(230,'307',21),(231,'308',21),(232,'309',21),(233,'404',21),(234,'405',21),(235,'406',21),(236,'407',21),(237,'504',21),(238,'505',21),(239,'508',21),(240,'605',21),(241,'607',21),(242,'1007',21),(243,'2007',21),(244,'2008',21),(245,'3008',21),(246,'Partner',21),(247,'Captur',22),(248,'Clio',22),(249,'Espace',22),(250,'Fluence',22),(251,'Grand Espace',22),(252,'Kadjar',22),(253,'Kangoo',22),(254,'Laguna',22),(255,'Megane',22),(256,'Modus',22),(257,'Rapid',22),(258,'Safrane',22),(259,'Scenic',22),(260,'Talisman',22),(261,'Thalia',22),(262,'Twingo',22),(263,'Altea',23),(264,'Altea XL',23),(265,'Cordoba',23),(266,'Ibiza',23),(267,'Leon',23),(268,'Toledo',23),(269,'900',24),(270,'9000',24),(271,'9-3',24),(272,'9-5',24),(273,'Crossblade',25),(274,'ForFour',25),(275,'ForTwo',25),(276,'Roadster',25),(277,'Forester',26),(278,'Impreza',26),(279,'Libero',26),(280,'Ignis',27),(281,'Liana',27),(282,'Maruti',27),(283,'Swift',27),(284,'Vitara',27),(285,'Fabia',28),(286,'Favorit',28),(287,'Felicia',28),(288,'Karoq',28),(289,'Kodiaq',28),(290,'Octavia',28),(291,'Praktik',28),(292,'Rapid',28),(293,'Roomster',28),(294,'Scala',28),(295,'Superb',28),(296,'Yeti',28),(297,'Auris',29),(298,'Avensis',29),(299,'Camry',29),(300,'Carina',29),(301,'Celica',29),(302,'Corolla',29),(303,'Corona',29),(304,'Hilux',29),(305,'Land Cruiser',29),(306,'Prius',29),(307,'RAV 4',29),(308,'Yaris',29),(309,'Amarok',30),(310,'Arteon',30),(311,'Bora',30),(312,'Caddy',30),(313,'EOS',30),(314,'Golf',30),(315,'Golf 1',30),(316,'Golf 2',30),(317,'Golf 3',30),(318,'Golf 4',30),(319,'Golf 5',30),(320,'Golf 6',30),(321,'Golf 7',30),(322,'Jetta',30),(323,'Lupo',30),(324,'Passat',30),(325,'Passat B1',30),(326,'Passat B2',30),(327,'Passat B3',30),(328,'Passat B4',30),(329,'Passat B5',30),(330,'Passat B5.5',30),(331,'Passat B6',30),(332,'Passat B7',30),(333,'Passat B8',30),(334,'Phaeton',30),(335,'Polo',30),(336,'Scirocco',30),(337,'Sharan',30),(338,'Tiguan',30),(339,'Touareg',30),(340,'Touran',30),(341,'Vento',30),(342,'240',31),(343,'245',31),(344,'340',31),(345,'440',31),(346,'460',31),(347,'480',31),(348,'740',31),(349,'745',31),(350,'760',31),(351,'850',31),(352,'940',31),(353,'Amazon',31),(354,'C30',31),(355,'C70',31),(356,'S40',31),(357,'S60',31),(358,'S70',31),(359,'S80',31),(360,'S90',31),(361,'V40',31),(362,'V50',31),(363,'V60',31),(364,'V70',31),(365,'XC60',31),(366,'XC70',31),(367,'XC90',31);
/*!40000 ALTER TABLE `models` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (30,'djordje@gmail.com','djordje','86dee38907b87fdb246ec54c712841cb','jeca'),(39,'bata@gmail.com','bata','484e8a9dd4a11596b5b70c50130025bf','batina'),(40,'zorandam@icloud.com','zoran','b8b94b05b0093affff30523e27291eca','zorandam');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'onlineshop'
--

--
-- Dumping routines for database 'onlineshop'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-12 14:55:22
